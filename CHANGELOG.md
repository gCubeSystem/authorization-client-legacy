This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for legacy Authorization Client

## [v2.0.8-SNAPSHOT]

- bom updated to 2.4.1

## [v2.0.7] - 2022-06-13

- Release for jenkins build